# 2048

Created by [Gabriele Cirulli](http://gabrielecirulli.com) ([original repository](https://github.com/gabrielecirulli/2048)). Based on [1024 by Veewo Studio](https://itunes.apple.com/us/app/1024!/id823499224) and conceptually similar to [Threes by Asher Vollmer](http://asherv.com/threes/).

**How to play**: Use your arrow keys to move the tiles. When two tiles with the same number touch, they merge into one!


